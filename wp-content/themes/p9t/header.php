<!doctype html>
<html <?php language_attributes();?>>
  <head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title><?php bloginfo('name');?> | 
        <?php is_front_page() ? bloginfo('description') : wp_title();?>
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome 5 CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    <?php wp_head(); ?>
  </head>
  <body>

<?php
if( is_front_page() || is_home() ){

}else{

}
/*
get_home_url();
get_bloginfo('template_url');
if( in_array(basename( get_page_template() ),array("page-gas.php","page-electric.php")) ){
*/
?>
<header>

</header>

<main role="main">